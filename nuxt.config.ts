// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  generate: {
    dir: "public",
  },
  nitro: {
    static: true,
  },
  routeRules: {
    // prerender index route by default
    "/": { prerender: true },
  },
  modules: ["@nuxtjs/robots", "@vueuse/nuxt", "nuxt-icons", "nuxt-windicss"],
  robots: {
    UserAgent: "*",
    Disallow: "/",
  },
  windicss: {
    analyze: false,
  },
});
