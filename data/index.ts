import type {
  BookItem,
  ExampleItem,
  LinkItem,
  SocialLinkItem,
  TechnologyItem,
} from "@/types";

export const books: Array<BookItem> = [
  {
    author: "Steve Krug",
    cover: "don_t-make-me-think.webp",
    href: "https://en.wikipedia.org/wiki/Don%27t_Make_Me_Think",
    summary:
      "Steve makes remembering that you don't have to over-complicate user interfaces, in fact, keeping them as simple as possible is always the best option.",
    title: "Don't make me think",
  },
  {
    author: "Marijn Haverbeke",
    cover: "eloquent-javascript.webp",
    href: "https://eloquentjavascript.net/",
    summary:
      "Just about the only resource you'll need if you want to learn about JavaScript. And the book list CC licensed.",
    title: "Eloquent JavaScript",
  },
  {
    author: "Daniel Kahneman",
    cover: "thinking-fast-and-slow.webp",
    href: "https://en.wikipedia.org/wiki/Thinking,_Fast_and_Slow",
    summary:
      "I particularly enjoyed this book as it helped me understand some of the behavioral science of our brains.",
    title: "Thinking, Fast and Slow",
  },
];

export const examples: Array<ExampleItem> = [
  {
    alt: "Example of a today screen",
    href: "https://codesandbox.io/s/q2bfm",
    image: "today-screen.png",
    label: "View example of Today screen on Codesandbox",
  },
  {
    alt: "Example of a responsive data table",
    href: "https://codesandbox.io/s/plrvh",
    image: "data-table.png",
    label: "View example of responsive data table on Codesandbox",
  },
  {
    alt: "Example of an accessible command palette",
    href: "https://codesandbox.io/s/elpgow",
    image: "command-palette.png",
    label: "View example of an accessible command palette on Codesandbox",
  },
  {
    alt: "Example of a people skills matrix",
    href: "https://codesandbox.io/s/4k93e",
    image: "people-skills.png",
    label: "View example of a people skills matrix on Codesandbox",
  },
];

export const links: Array<LinkItem> = [
  {
    href: "#intro",
    icon: "info",
    label: "Introduction",
  },
  {
    href: "#experience",
    icon: "work",
    label: "Experience",
  },
  {
    href: "#examples",
    icon: "file-code",
    label: "Examples",
  },
  {
    href: "#technologies",
    icon: "technologies",
    label: "Technologies",
  },
  {
    href: "#links",
    icon: "links",
    label: "Social links",
  },
];

export const socialLinks: Array<SocialLinkItem> = [
  {
    href: "https://github.com/neilmerton",
    icon: "github",
    label: "GitHub",
  },
  {
    href: "https://gitlab.com/neilmerton",
    icon: "gitlab",
    label: "GitLab",
  },
  {
    href: "https://www.linkedin.com/in/neilmerton/",
    icon: "linkedin",
    label: "LinkedIn",
  },
  {
    href: "https://mas.to/@neilmerton",
    icon: "mastodon",
    label: "Mastodon",
  },
  {
    href: "https://stackoverflow.com/users/9127864/?tab=profile",
    icon: "stack-overflow",
    label: "Stack Overflow",
  },
  {
    href: "https://www.strava.com/athletes/33575879",
    icon: "strava",
    label: "Strava",
  },
  {
    href: "https://twitter.com/neilmerton",
    icon: "twitter",
    label: "Twitter",
  },
];

export const technologies: Array<TechnologyItem> = [
  {
    icon: "astro",
    label: "Astro",
  },
  {
    icon: "css",
    label: "CSS",
  },
  {
    icon: "js",
    label: "JavaScript",
  },
  {
    icon: "html",
    label: "HTML",
  },
  {
    icon: "nuxt",
    label: "Nuxt",
  },
  {
    icon: "postcss",
    label: "PostCSS",
  },
  {
    icon: "sass",
    label: "Sass",
  },
  {
    icon: "typescript",
    label: "TypeScript",
  },
  {
    icon: "vue",
    label: "Vue.js",
  },
  {
    icon: "vite",
    label: "Vite",
  },
  {
    icon: "webpack",
    label: "Webpack",
  },
  {
    icon: "windicss",
    label: "Windi CSS",
  },
];
