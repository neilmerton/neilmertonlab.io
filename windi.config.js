export default {
  darkMode: "class",
  theme: {
    extend: {
      fontFamily: {
        body: ["IBM Plex Mono", "system-ui"],
      },
    },
  },
};
