export type BookItem = {
  author: string;
  cover: string;
  href: string;
  summary: string;
  title: string;
};

export type ExampleItem = {
  alt: string;
  href: string;
  image: string;
  label: string;
};

export type LinkItem = {
  href: string;
  icon: string;
  label: string;
};

export type SocialLinkItem = {
  href: string;
  icon: string;
  label: string;
};

export type TechnologyItem = {
  icon: string;
  label: string;
};
